from app import (
	app,
	mysql,
)
from app.models.data import (
	Airport,
	Customer,
	Employee,
	Person
)
from app.models.session import Session
from random import choice

# Returns (index, is_valid) if a button was pressed
def get_search(forms):
	for i in range(len(forms)):
		if forms[i].search.data:
			return i, True
	return 0, False

# Returns (index, is_valid) if a button was pressed
def get_reserve(forms):
	for i in range(len(forms)):
		if forms[i].reserve.data:
			return i, True
	return 0, False

# Populate the fields of the Account form
def get_account_data(account_form, user, person, employee):
	if person:
		account_form.firstName.data = person.get('FirstName', '')
		account_form.lastName.data = person.get('LastName', '')
		account_form.streetAddress.data = person.get('Address', '')
		account_form.city.data = person.get('City', '')
		account_form.state.data = person.get('State', '')
		account_form.zipCode.data = person.get('ZipCode', '')

	if user:
		account_form.email.data = user.get('Email', '')
		account_form.creditCard.data = user.get('CreditCardNo', '')
		account_form.accountNo.data = user.get('AccountNo', '')

	if employee:
		account_form.ssn.data = employee.get('SSN', '')
		account_form.startDate.data = employee.get('StartDate', '')
		account_form.hourlyRate.data = employee.get('HourlyRate', '')

def get_all_user_data(pid):
	pid = int(pid)

	results = query(Customer.GET, args=[pid])
	user = results[0] if results else {}

	results = query(Person.GET, args=[pid])
	person = results[0] if results else {}

	results = query(Employee.GET, args=[pid])
	employee = results[0] if results else {}

	return user, person, employee

def get_all_items(statement, args=None, skip=None):
	results = query(statement, args)
	names = []

	for result in results:
		for _, v in result.items():
			if skip and skip == v:
				continue
			names.append((str(v), v))

	return sorted(names, key=lambda x: x[1])

def populate_flights(all_flights):
	all_choices = []

	for flights in all_flights:
		choices = []

		for i in range(len(flights)):
			flight = flights[i]
			text = flight.get('AirlineID', '') + ' ' + str(flight.get('FlightNo', ''))
			choices.append((i, text))
		all_choices.append(choices)

	Session.temp(data={'flights': all_flights, 'choices': all_choices})
	print(Session.temp())

def get_reservation_choices(reservations):
	choices = []

	for i in range(len(reservations)):
		res = reservations[i]
		choices.append((i, res.get('ResrNo', i)))

	return choices

def get_days_of_week(days_map):
	days = ['Sunday', 'Monday', 'Tuesday',
		'Wednesday', 'Thursday', 'Friday', 'Saturday']
	out = ''

	for i in range(len(days_map)):
		day = days_map[i]
		if day == '1':
			out += days[i] + ', '

	return out[:len(out)-2]

def get_random_employee_id():
	rep_id = None
	results = query(Employee.GET_ALL_IDS)

	if results and len(results) > 1:
		while rep_id == str(Session.user().get('Id')):
			rep_id = choice(results)['Id']

	if not rep_id:
		rep_id = 4

	return rep_id

# Perform a query and return the result
def query(query, args=None):
	cursor, data = None, None

	print('Performing query: %s' % (query))

	if args:
		for i in range(len(args)):
			args[i] = str(args[i])

	try:
		cursor = mysql.connection.cursor()
		if args:
			cursor.execute(query, args)
		else:
			cursor.execute(query)
		data = cursor.fetchall()
	except Exception as e:
		print('query() error:', e, '- args:', args)
		return None
	finally:
		if cursor:
			cursor.close()

	print('query() got (%d) results back' % (len(data)))
	return data

def transact(transaction, args):
	conn, cursor, data = None, None, None

	print('Performing transaction: %s' % (transaction))

	for i in range(len(args)):
		args[i] = str(args[i])

	try:
		conn = mysql.connection
		cursor = conn.cursor()
		cursor.execute(transaction, args)
		conn.commit()
		data = cursor.lastrowid
	except Exception as e:
		print('transact() error:', e, '- args:', args)
		return None
	finally:
		if cursor:
			cursor.close()

	print('transact() row ID: %s' % (data))
	return data