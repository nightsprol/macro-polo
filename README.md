# Macro Polo Travels

Simple travel website based on Expedia.

# Setup

To begin, you should clone the repository.
Assuming you are using `ssh`, you should do the following:

```sh
$ git clone git@gitlab.com:nightsprol/macro-polo.git
$ cd macro-polo
```

Next you need to install a couple dependencies.
If you do not have Python, download the latest version of Python 3 and add it to your `PATH`.
Python comes with `pip` which can be used to install dependencies easily.

You will need to get a few Flask packages:

```sh
$ pip install flask
$ pip install flask_wtf
$ pip install flask_mysqldb
```

Now you should be ready to run the server!

# Running

To properly run the server you must open it using Python.
Assuming you are in the root directory for the repository `macro-polo/`:

```sh
$ python run.py
```

The above will start up the server in your terminal.
Next all you need to do is open your browser and go to `http://localhost:5000/`.
If everything worked you should see the homepage of the website.
As you perform actions on it, your server should write back information about those actions in your terminal.

# Directory Layout

If you want to make changes, they should all be done in `./app/`.

- To add server endpoints, modify `./app/views.py`.
- To create webpages, add or modify files in `./app/templates/`.
- To create data objects, add or modify files in `./app/models/`.

# MySQL Server

You should download and unpack MySQL and add the `bin` directory to your `PATH`.
To access the server, run it with the following command:

```sh
$ mysql -h <hostname> -u <username> -p
```

It will then prompt you for the password.
The information for each of those fields is listed below:

| Hostname                        | Username      | Password      |
| ------------------------------- | ------------- | ------------- |
| sql9.freemysqlhosting.net       | sql9208544    | 8aGYK4uVQM    |

There is a resource on how to use MySQL with Flask
[here](https://code.tutsplus.com/tutorials/creating-a-web-app-from-scratch-using-python-flask-and-mysql--cms-22972).

# Authors

- Shaiv Patel
- Andrew Raymond
- Nathan Wood