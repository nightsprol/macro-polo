from app import app
from app.helpers import (
	get_account_data,
	get_all_items,
	get_all_user_data,
	get_days_of_week,
	get_random_employee_id,
	get_reserve,
	get_reservation_choices,
	get_search,
	populate_flights,
	query,
	transact,
)
from app.models.data import *
from app.models.forms import *
from app.models.session import Session
from flask import (
	flash,
	json,
	redirect,
	render_template,
	request,
	session,
)
from flask_mysqldb import MySQL
from werkzeug import (
	check_password_hash,
	generate_password_hash,
)

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
	round_trip	= RoundTripSearchForm(prefix='round_trip')
	one_way		= OneWaySearchForm(prefix='one_way')
	multi_city	= MultiCitySearchForm(prefix='multi_city')
	rt_Reserve 	= ReserveForm(prefix='rt_Reserve')
	ow_Reserve 	= ReserveForm(prefix='ow_Reserve')
	mc_Reserve 	= ReserveForm(prefix='mc_Reserve')

	index = 0
	flights = [[], [], []]
	forms = [round_trip, one_way, multi_city]
	reserves = [rt_Reserve, ow_Reserve, mc_Reserve]
	searched = False
	reserved = False

	airports = get_all_items(Airport.GET_IDS)
	ids = [(str(Session.user().get('Id')), 'My Account')]

	if Session.is_employee():
		ids.extend(get_all_items(Employee.GET_CUSTOMERS, args=[Session.user().get('Id')]))

	for form in forms:
		form.origin.choices = airports

		for entry in form.flights:
			entry.destination.choices = airports

	for reserve in reserves:
		for entry in reserve.flights:
			if Session.temp().get('choices'):
				entry.flight.choices = Session.temp().get('choices').pop(-1)
		reserve.accounts.choices = ids

	if request.method == 'POST':
		if multi_city.add_flight.data:
			multi_city.flights.append_entry(FormField(FlightEntryForm))

			# Refresh the data
			for entry in multi_city.flights:
				entry.destination.choices = airports

			index = 2
		else:
			index, is_valid = get_search(forms)
			res_i, res_valid = get_reserve(reserves)

			if is_valid:
				searched = True

				if index == 0:
					flights[0].append(query(Flight.GET_OUTGOING, args=[
						round_trip.origin.data,
						round_trip.flights[0].destination.data,
						round_trip.flights[0].departing.data,
						round_trip.numPass.data,
					]))

					flights[0].append(query(Flight.GET_OUTGOING, args=[
						round_trip.flights[0].destination.data,
						round_trip.origin.data,
						round_trip.returning.data,
						round_trip.numPass.data,
					]))

					for _flights in flights[0]:
						for flight in _flights:
							flight['DaysOperating'] = get_days_of_week(flight['DaysOperating'])
					populate_flights(flights[0])

					rt_Reserve.flights.append_entry(FormField(SelectFlightForm))

				elif index == 1:
					flights[1].append(query(Flight.GET_OUTGOING, args=[
						one_way.origin.data,
						one_way.flights[0].destination.data,
						one_way.flights[0].departing.data,
						one_way.numPass.data,
					]))

					for _flights in flights[1]:
						for flight in _flights:
							flight['DaysOperating'] = get_days_of_week(flight['DaysOperating'])
					populate_flights(flights[1])

				elif index == 2:
					flights[2].append(query(Flight.GET_OUTGOING, args=[
						multi_city.origin.data,
						multi_city.flights[0].destination.data,
						multi_city.flights[0].departing.data,
						multi_city.numPass.data,
					]))

					for i in range(1, len(multi_city.flights)):
						flights[2].append(query(Flight.GET_OUTGOING, args=[
							multi_city.flights[i-1].destination.data,
							multi_city.flights[i].destination.data,
							multi_city.flights[i].departing.data,
							multi_city.numPass.data,
						]))

						mc_Reserve.flights.append_entry(FormField(SelectFlightForm))

					for _flights in flights[2]:
						for flight in _flights:
							flight['DaysOperating'] = get_days_of_week(flight['DaysOperating'])
					populate_flights(flights[2])

			elif res_valid:

				account_no = Session.user().get('AccountNo')
				rep_id = Session.user().get('RepresentativeId')

				# Let's make a reservation
				resr_no = transact(Reservation.CREATE, args=[
					'50',
					rep_id,
					account_no,
				])

				# Create an includes for each leg of the trip :)
				for i in range(len(reserves[res_i].flights)):
					entry = reserves[res_i].flights[i]
					flight = Session.temp().get('flights')[i][int(entry.flight.data)]

					transact(Reservation.CREATE_PART2, args=[
						resr_no,
						flight['AirlineID'],
						flight['FlightNo'],
						flight['LegNo'],
					])

				flash('The reservation was successfully made')
				return redirect('/reservations')

	# Refresh the data
	for reserve in reserves:
		for i in range(len(reserve.flights)):
			entry = reserve.flights[i]

			if Session.temp().get('choices'):
				entry.flight.choices = Session.temp().get('choices')[i]
		reserve.accounts.choices = ids

	return render_template("index.html",
		title='Home',
		user=Session.user(),
		round_trip=round_trip,
		one_way=one_way,
		multi_city=multi_city,
		rt_Reserve=rt_Reserve,
		ow_Reserve=ow_Reserve,
		mc_Reserve=mc_Reserve,
		active=index,
		round_trip_flights=[(flight, select) for flight, select in zip(flights[0], rt_Reserve.flights)],
		one_way_flights=[(flight, select) for flight, select in zip(flights[1], ow_Reserve.flights)],
		multi_city_flights=[(flight, select) for flight, select in zip(flights[2], mc_Reserve.flights)],
		searched=searched,
	)

@app.route('/login', methods=['GET', 'POST'])
@Session.require_logged_out('/account')
def login():
	form = LoginForm()

	if request.method == 'POST':
		if form.register.data:
			Session.temp(data={'Email': form.email.data})
			return redirect('/register')

		elif form.validate_on_submit():

			# Query the DB for the email
			results = query(Customer.GET_BY_EMAIL, args=[form.email.data])

			if not results:
				flash('Login failed, no account exists with that email')
				return render_template('login.html', form=form)

			user = results[0]
			person = {}

			if not check_password_hash(user['Password'], form.password.data):
				flash('Login failed, invalid password')
				return render_template('login.html', form=form)

			results = query(Person.GET, args=[user['Id']])

			if results:
				person = results[0]

			employee = {}

			results = query(Employee.GET, args=[user['Id']])

			if results:
				employee = results[0]

			Session.login(user, person, employee)
			flash('Login was successful')
			return Session.redirect_to_source()

	return render_template('login.html',
		title='Login',
		form=form,
	)

@app.route('/register', methods=['GET', 'POST'])
@Session.require_logged_out('/account')
def register():
	form = RegisterForm()

	if request.method == 'POST' and form.validate_on_submit():
		# Check if email exists
		if query(Customer.GET_BY_EMAIL, args=[form.email.data]):
			flash('An account with that email already exists')
			return render_template('register.html', title='Register', form=form)

		_id = transact(Person.CREATE, args=[
			form.firstName.data,
			form.lastName.data,
			form.streetAddress.data,
			form.city.data,
			form.state.data,
			form.zipCode.data,
		])

		if not _id:
			flash('Registration for person failed!')
			return render_template('register.html', title='Register', form=form)

		_account_no = transact(Customer.CREATE, args=[
			_id,
			form.email.data,
			generate_password_hash(form.password.data),
			form.creditCard.data,
			str(get_random_employee_id()),
		])

		if not _account_no:
			flash('Registration for customer failed')
			return render_template('register.html', title='Register', form=form)

		flash('Register succeeded for name="%s %s", email="%s"' %
			(form.firstName.data, form.lastName.data, form.email.data))
		return redirect('/index')

	form.email.data = Session.temp().get('Email', '')
	return render_template('register.html',
		title='Register',
		form=form,
	)

@app.route('/logout', methods=['GET'])
@Session.require_login('/logout')
def logout():
	flash('User "%s" logged out' % (Session.user().get('Email', '')))
	Session.logout()
	return redirect('/index')

@app.route('/account', methods=['GET', 'POST'])
@Session.require_login('/account')
def account():
	form = AccountForm()

	form.accounts.choices = [(str(Session.user().get('Id')), 'My Account')]
	user, person, employee = get_all_user_data(Session.user().get('Id'))

	if Session.is_manager():
		ids = get_all_items(Employee.GET_ALL_IDS, skip=Session.user().get('Id'))
		form.accounts.choices.extend(ids)
	elif Session.is_employee():
		ids = get_all_items(Employee.GET_CUSTOMERS, args=[Session.user().get('Id')])
		form.accounts.choices.extend(ids)

	if request.method == 'POST':
		if form.save.data and form.validate_on_submit():
			transact(Person.EDIT, args=[
				form.firstName.data,
				form.lastName.data,
				form.streetAddress.data,
				form.city.data,
				form.state.data,
				form.zipCode.data,
				form.accounts.data,
			])

			transact(Customer.EDIT, args=[
				form.creditCard.data,
				form.accounts.data,
			])

			flash('Account data successfully edited')
			user, person, employee = get_all_user_data(form.accounts.data)

			if form.accounts.data == str(Session.user().get('Id')):
				Session.login(user, person, employee)

		elif form.delete.data:
			transact(Customer.DELETE, [form.accounts.data])
			transact(Person.DELETE, [form.accounts.data])
			transact(Employee.DELETE, [form.accounts.data])
			flash('Account succesfully deleted')

			if form.accounts.data == str(Session.user().get('Id')):
				return redirect('/logout')
		elif form.switchAccounts.data:
			user, person, employee = get_all_user_data(form.accounts.data)

	get_account_data(form, user, person, employee)

	return render_template('account.html',
		title='Account',
		user=Session.user(),
		form=form,
	)

@app.route('/reservations', methods=['POST', 'GET'])
@Session.require_login('/reservations')
def reservations():
	account_no = Session.user().get('AccountNo')

	pending_form = PendingReservations()
	current_form = CurrentReservations()

	pending_reservations = query(Reservation.GET_BY_CUSTOMER_PENDING, args=[account_no])
	current_reservations = query(Reservation.GET_BY_CUSTOMER_ACCEPTED, args=[account_no])
	past_reservations = query(Reservation.GET_PAST, args=[account_no])
	bid_history = query(Bid.GET_BY_ACCOUNTNO, args=[account_no])

	# Populate drop down fields
	pending_form.reservation.choices = get_reservation_choices(pending_reservations)
	current_form.reservation.choices = get_reservation_choices(current_reservations)

	if request.method == 'POST':
		if pending_form.make_bid.data:
			res_i = int(pending_form.reservation.data)
			bid = float(pending_form.bid.data)

			# Get the selected reservation
			res = pending_reservations[res_i]
			resr_no = res.get('ResrNo')

			# Check if this bid should be accepted
			results = query(Flight.GET_HIDDEN_FARE, args=[resr_no])
			fare = float(results[0]['HiddenFare']) if results else bid + 1
			accepted = bid >= fare

			if not accepted:
				flash('The airline did not accept your bid!')
			else:
				flash('The airline accepted your bid.')

			# Perform bid yo
			result = transact(Bid.ADD, args=[
				pending_form.bid.data,
				1 if accepted else 0,
				resr_no,
			])

			return redirect('/reservations')

		elif current_form.cancel.data:

			# Get the selected reservation
			res = current_reservations[int(current_form.reservation.data)]

			# Delete related rows
			result = transact(Reservation.DELETE_BIDS, args=[
				res.get('ResrNo'),
			])
			result = transact(Reservation.DELETE_INCLUDES, args=[
				res.get('ResrNo'),
			])

			# Cancel reservation my man
			result = transact(Reservation.DELETE, args=[
				res.get('ResrNo'),
				Session.user().get('AccountNo'),
			])

			flash('Cancelled the reservation')
			return redirect('/reservations')

	return render_template('reservations.html',
		title='Reservations',
		user=Session.user(),
		pending_form=pending_form,
		current_form=current_form,
		pending_reservations=pending_reservations,
		current_reservations=current_reservations,
		past_reservations=past_reservations,
		bid_history=bid_history,
	)

@app.route('/auctions', methods=['GET'])
@Session.require_login('/auctions')
def auctions():
	return render_template('404.html',
		title='Auctions',
		user=Session.user(),
	)

@app.errorhandler(404)
def page_not_found(e):
	return render_template('404.html',
		title='Page Not Found',
		user=Session.user(),
		), 404

'''
	Employee Level Pages
'''

@app.route('/customers', methods=['GET', 'POST'])
@Session.require_employee()
def customers():
	if not Session.is_manager():
		mailing_list = query(Employee.GET_MAILING_LIST, args=[Session.user().get('Id')])

		return render_template("customers.html",
			title='Customers',
			user=Session.user(),
			mailing_list=mailing_list,
		)

	customerManager	= CustomerManager(prefix='customerManager')
	reservationManager	= ReservationManager(prefix='reservationManager')

	airline_ids = get_all_items(Flight.GET_ALL_AIRLINEID)
	flight_nos = get_all_items(Flight.GET_ALL_FLIGHTNO)

	index = 0

	forms = [customerManager, reservationManager]
	queries = [Customer.GET_BY_FLIGHT, Reservation.GET_BY_FLIGHT]
	results = [None, None]

	for form in forms:
		form.airlineID.choices = airline_ids
		form.flightNo.choices = flight_nos

	if request.method == 'POST':
		index, is_valid = get_search(forms)

		if is_valid:
			results[index] = query(queries[index], args=[
				forms[index].airlineID.data,
				forms[index].flightNo.data,
			])

	return render_template("customers.html",
		title='Customers',
		user=Session.user(),
		active=index,
		customerManager=customerManager,
		reservationManager=reservationManager,
		customers=results[0],
		reservations=results[1],
	)

'''
	Manager Level Pages
'''

@app.route('/flights', methods=['GET', 'POST'])
@Session.require_manager()
def flights():
	byCustomer = ByCustomerForm(prefix='0')
	byAirport = ByAirportForm(prefix='1')

	index = 0
	flights = [None, None]
	forms = [byCustomer, byAirport]

	byAirport.airport.choices = get_all_items(Airport.GET_IDS)

	if request.method == 'POST':
		index, is_valid = get_search(forms)

		print(index, is_valid)

		if is_valid:
			if index == 0:
				flights[index] = query(Flight.GET_BY_CUSTOMER, args=[
					byCustomer.accountNo.data,
				])
			elif index == 1:
				flights[index] = query(Flight.GET_BY_AIRPORT, args=[
					byAirport.airport.data, byAirport.airport.data
				])

	return render_template("flights.html",
		title='Flights',
		user=Session.user(),
		byCustomer=byCustomer,
		byAirport=byAirport,
		active=index,
		customer_flights=flights[0],
		airport_flights=flights[1],
	)

@app.route('/revenue', methods=['GET', 'POST'])
@Session.require_manager()
def revenue():
	flightRevenue = FlightRevenue(prefix='flightRevenue')
	cityRevenue = CityRevenue(prefix='cityRevenue')
	customerRevenue = CustomerRevenue(prefix='customerRevenue')
	salesReport = SalesReport(prefix='salesReport')
	index = 0
	revenue = [None, None, None,None]
	forms = [flightRevenue,cityRevenue,customerRevenue,salesReport]
	searched = False

	flightRevenue.airlineID.choices = get_all_items(Flight.GET_ALL_AIRLINEID)
	flightRevenue.flightNo.choices = get_all_items(Flight.GET_ALL_FLIGHTNO)

	cityRevenue.destCity.choices = get_all_items(Airport.GET_CITIES)

	if request.method == 'POST':
		index, is_valid = get_search(forms)
		searched = True

		if is_valid:
			if index == 0:
				results = query(Revenue.GET_BY_FLIGHT, args=[
					flightRevenue.flightNo.data,
					flightRevenue.airlineID.data,
				])

				revenue[index] = results[0].get('BidTotal') if results else None
			elif index == 1:
				results = query(Revenue.GET_BY_DESTINATION, args=[
					cityRevenue.destCity.data,
				])

				revenue[index] = results[0].get('BidTotal') if results else None
			elif index == 2:
				results = query(Revenue.GET_BY_CUSTOMER, args=[
					customerRevenue.accountNo.data,
				])

				revenue[index] = results[0].get('BidTotal') if results else None
			elif index == 3:
				revenue[index] = query(Revenue.GET_BY_MONTHINYEAR, args=[
					salesReport.month.data,
					salesReport.month.data,
					salesReport.year.data,
					salesReport.year.data,
				])

	return render_template("revenue.html",
		title='Revenue',
		user=Session.user(),
		salesReport=salesReport,
		active=index,
		flightRevenue=flightRevenue,
		cityRevenue=cityRevenue,
		customerRevenue=customerRevenue,
		flight_revenue=revenue[0],
		city_revenue=revenue[1],
		customer_revenue=revenue[2],
		sales_report=revenue[3],
		searched=searched,
	)

@app.route('/database', methods=['GET', 'POST'])
@Session.require_manager()
def database():
	test_form = TestDBForm()
	results = None

	if request.method == 'POST' and test_form.execute.data:
		_proc = test_form.procs.data
		_statement = test_form.statements.data
		_args = None

		if not test_form.args.data == '':
			_args = test_form.args.data.split(', ')

		if _proc == 'query':
			results = query(eval(_statement), args=_args)
			if not results:
				flash('query() found no results')
		elif _proc == 'transact':
			_res = transact(eval(_statement), _args)
			flash('transact() returned %s' % (str(_res)))

	return render_template('/database.html',
		title='Database',
		user=Session.user(),
		form=test_form,
		results=results,
	)