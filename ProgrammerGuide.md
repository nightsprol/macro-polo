# Macro Polo Travels: Programmer Guide
1. [Contents](#contents)
2. [Front-End](#front-end)
	1. [GUI Overview](#gui)
	2. [Website Explanation](#webexp)
3. [Back-End](#back-end)
	1. [MySQL Queries](#queries)
	2. [Connecting the GUI To the Queries](#connection)

# Contents <a name "contents"></a>
The contents of this website and database are structured as follows:

		app
			models
				data.py
				forms.py
				session.py
			static
				js
					jquery-3.2.1.js
					scripts.js
				styles
					flightSearch.css
					signup.css
					style.css
			templates
				_formhelpers.html
				404.html
				account.html
				base.html
				customers.html
				database.html
				flights.html
				index.html
				login.html
				register.html
				reservations.html
				revenue.html
			__init__.py
			helpers.py
			views.py
		.gitIgnore
		config.py
		ProgrammerGuide.md
		README.md
		run.py 

# Front-End <a name "front-end"></a>

	

# Back-End <a name "back-end"></a>
MySQL Queries: <a name "queries"></a>
	These queries were created all inside of the data.py file in order to keep track of them easily. They are organized based on their function as such: Person, Customer, Employee, Flight, Reservation, Airport, Revenue, Bid. The following functions are the ones used in the front-end of the website - the rest being used in the background in order to get information not pertinent to the front end users. These are organized based on their page of the website.

		Queries and Transactions needed in the GUI/Front-end
		----------------------------------------------------
		Home Page:
			Flight.GET_ROUNDTRIP					(DepAirportId CHAR, ArrAirportId CHAR, Departure DATE, ArrAirportId CHAR, DepAirportId CHAR, Return DATE, NoOfSeats INTEGER)
			Flight.GET_OUTGOING 					(DepAirportId CHAR, ArrAirportId CHAR, Departure DATE, NoOfSeats INTEGER)
			Flight.GET_MULTICITY					(DepAirportId CHAR, NoOfSeats INTEGER, Departure DATE, NoOfSeats INTEGER)

		Reservations Page:
			Reservation.GET_CURRENT					(AccountNo INTEGER)
			Reservation.GET_PAST					(AccountNo INTEGER)
			Reservation.GET_BY_CUSTOMER 			(AccountNo INTEGER)
			Reservation.GET_BY_CUSTOMER_PENDING 	(AccountNo INTEGER)
			Reservation.GET_BY_CUSTOMER_ACCEPTED 	(AccountNo INTEGER)
			Reservation.CREATE 						(ResrNo INTEGER, BookingFee FLOAT, RepId INTEGER, AccountNo INTEGER)
			Reservation.CREATE_PART2				(ResrNo INTEGER, AirlineID CHAR, FlightNo INTEGER, LegNo INTEGER)

			Bid.GET 								(BidId INTEGER)
			Bid.GET_BY_ACCOUNTNO 					(AccountNo INTEGER)
			Bid.GET_ALL 							(NONE)
			Bid.ADD 								(Bid FLOAT, Accepted TINYINT (a 1 or 0), ResrNo INTEGER)

			Flight.GET_HIDDEN_FARE					(ResrNo INTEGER)

		Customers Page:
			Customer.GET_BY_FLIGHT	 	 			(AirlineId INTEGER, FlightNo INTEGER)
			Reservation.GET_BY_FLIGHT				(AirlineId INTEGER, FlightNo INTEGER)

		Flights Page:
			Flight.GET_BY_CUSTOMER					(AccountNo INTEGER)
			Flight.GET_BY_AIRPORT					(AirportId INTEGER, AirportId INTEGER)

		Revenue Page:
			Revenue.GET_BY_FLIGHT					(FlightNo INTEGER, AirlineID CHAR(2))
			Revenue.GET_BY_DESTINATION  			(City CHAR(16))
			Revenue.GET_BY_CUSTOMER					(AccountNo INTEGER)
			Revenue.GET_BY_MONTHINYEAR				(DateBought DATETIME)

		Account Page:
			Person.GET 	 							(Id INTEGER)
			Customer.GET 							(Id INTEGER)
			Employee.GET 							(Id INTEGER)

			Person.EDIT 							(FirstName VARCHAR, LastName VARCHAR, Address VARCHAR, City VARCHAR, State VARCHAR, Zipcode INTEGER, Id INTEGER)
			Customer.EDIT 							(CreditCardNo INTEGER, Id INTEGER)
			Employee.EDIT 							(Id INTEGER, UserSSN INTEGER, IsManager BOOLEAN, StartDate DATE, HourlyRate INTEGER)

		Revenue Page:
			Revenue.GET_ALL 						(NONE)
			Revenue.GET_BY_MONTH_IN_YEAR 			(MonthBought MONTH, MonthBought MONTH, YearBought YEAR, YearBought YEAR)
			Revenue.GET_BY_ID 						(BidID INTEGER)
			Revenue.GET_BY_FLIGHT					(FlightNo INTEGER, AirlineID CHAR(2))
			Revenue.GET_BY_DESTINATION 				(City CHAR(16))
			Revenue.GET_BY_CUSTOMER 				(AccountNo INTEGER)

Connecting Back-end to Website: <a name "connection"></a>
	Stuff about the connections




