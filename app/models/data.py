'''
Queries and Transactions needed in the GUI/Front-end
----------------------------------------------------------------------------
Home Page:
	Flight.GET_ROUNDTRIP					(DepAirportId CHAR, ArrAirportId CHAR, Departure DATE, ArrAirportId CHAR, DepAirportId CHAR, Return DATE, NoOfSeats INTEGER)
	Flight.GET_OUTGOING 					(DepAirportId CHAR, ArrAirportId CHAR, Departure DATE, NoOfSeats INTEGER)
	Flight.GET_MULTICITY					(DepAirportId CHAR, NoOfSeats INTEGER, Departure DATE, NoOfSeats INTEGER)

Reservations Page:
	Reservation.GET_CURRENT					(AccountNo INTEGER)
	Reservation.GET_PAST					(AccountNo INTEGER)
	Reservation.GET_BY_CUSTOMER 			(AccountNo INTEGER)
	Reservation.GET_BY_CUSTOMER_PENDING 	(AccountNo INTEGER)
	Reservation.GET_BY_CUSTOMER_ACCEPTED 	(AccountNo INTEGER)
	Reservation.CREATE 						(ResrNo INTEGER, BookingFee FLOAT, RepId INTEGER, AccountNo INTEGER)
	Reservation.CREATE_PART2				(ResrNo INTEGER, AirlineID CHAR, FlightNo INTEGER, LegNo INTEGER)

	Bid.GET 								(BidId INTEGER)
	Bid.GET_BY_ACCOUNTNO 					(AccountNo INTEGER)
	Bid.GET_ALL 							(NONE)
	Bid.ADD 								(Bid FLOAT, Accepted TINYINT (a 1 or 0), ResrNo INTEGER)

	Flight.GET_HIDDEN_FARE					(ResrNo INTEGER)

Customers Page:
	Customer.GET_BY_FLIGHT	 	 			(AirlineId INTEGER, FlightNo INTEGER)
	Reservation.GET_BY_FLIGHT				(AirlineId INTEGER, FlightNo INTEGER)

Flights Page:
	Flight.GET_BY_CUSTOMER					(AccountNo INTEGER)
	Flight.GET_BY_AIRPORT					(AirportId INTEGER, AirportId INTEGER)

Revenue Page:
	Revenue.GET_BY_FLIGHT					(FlightNo INTEGER, AirlineID CHAR(2))
	Revenue.GET_BY_DESTINATION  			(City CHAR(16))
	Revenue.GET_BY_CUSTOMER					(AccountNo INTEGER)
	Revenue.GET_BY_MONTHINYEAR				(DateBought DATETIME)

Account Page:
	Person.GET 	 							(Id INTEGER)
	Customer.GET 							(Id INTEGER)
	Employee.GET 							(Id INTEGER)

	Person.EDIT 							(FirstName VARCHAR, LastName VARCHAR, Address VARCHAR, City VARCHAR, State VARCHAR, Zipcode INTEGER, Id INTEGER)
	Customer.EDIT 							(CreditCardNo INTEGER, Id INTEGER)
	Employee.EDIT 							(Id INTEGER, UserSSN INTEGER, IsManager BOOLEAN, StartDate DATE, HourlyRate INTEGER)

Revenue Page:
	Revenue.GET_ALL 						(NONE)
	Revenue.GET_BY_MONTH_IN_YEAR 			(MonthBought MONTH, MonthBought MONTH, YearBought YEAR, YearBought YEAR)
	Revenue.GET_BY_ID 						(BidID INTEGER)
	Revenue.GET_BY_FLIGHT					(FlightNo INTEGER, AirlineID CHAR(2))
	Revenue.GET_BY_DESTINATION 				(City CHAR(16))
	Revenue.GET_BY_CUSTOMER 				(AccountNo INTEGER)

'''


'''
OVERVIEW
========
All work!
'''
class Person:
	'''
		Description: 	Return a person
		Parameters: 	Id INTEGER
		Tested:			Works
	'''
	GET = \
		'''
			SELECT 	*
			FROM 	Person
			WHERE 	Id = %s;
		'''

	'''
		Description: 	Create a person
		Parameters: 	FirstName VARCHAR, LastName VARCHAR, Address VARCHAR, City VARCHAR, State VARCHAR, Zipcode INTEGER
		Tested:			Works
	'''
	CREATE = \
		'''
			INSERT INTO Person(FirstName, LastName, Address, City, State, ZipCode)
			VALUES(%s, %s, %s, %s, %s, %s);
		'''

	'''
		Description: 	Edit a person
		Parameters: 	FirstName VARCHAR, LastName VARCHAR, Address VARCHAR, City VARCHAR, State VARCHAR, Zipcode INTEGER, Id INTEGER
		Tested:			Works
	'''
	EDIT = \
		'''
			UPDATE Person
				SET 	FirstName 	= %s,
						LastName 	= %s,
						Address 	= %s,
						City	 	= %s,
						State 	 	= %s,
						ZipCode 	= %s
				WHERE 	Id 			= %s;
		'''

	'''
		Description: 	Delete a person
		Parameters: 	Id INTEGER
		Tested:			Works
	'''
	DELETE = \
		'''
			DELETE FROM Person
			WHERE Id = %s
		'''


'''
OVERVIEW
========
All work!
'''
class Customer:
	'''
		Description:	Returns all customers
		Parameters:		None
		Tested:			Works
	'''
	GET_ALL = \
		'''
			SELECT * FROM Customer;
		'''

	'''
		Description:	Returns all customers by Id
		Parameters:		Id INTEGER
		Tested:			Nope
	'''
	GET = \
		'''
			SELECT * FROM Customer
			WHERE Id = %s;
		'''

	'''
	Description:		Get an accountno by personId
	Parameters:			Id INTEGER
	'''
	GET_ACCOUNTNO_BY_ID = \
		'''
			SELECT AccountNo FROM Customer
			WHERE Id = %s;
		'''

	'''
		Description: 	Return the user account
		Parameters: 	Email STRING
		Tested:			Works
	'''
	GET_BY_EMAIL = \
		'''
			SELECT 	*
			FROM 	Customer
			WHERE 	Email = %s;
		'''

	'''
		Description: 	Get all users that are in a current flight
		Parameters:		AirlineId INTEGER, FlightNo INTEGER
		Tested:			Works
	'''
	GET_BY_FLIGHT = \
		'''
		SELECT 	P.FirstName, P.LastName, C.AccountNo
		FROM 	Reservation R, Includes I, Person P, Customer C
		WHERE 	R.ResrNo 		= 	I.ResrNo 	AND
			   	P.ID 			= 	C.ID 		AND
			   	C.AccountNo 	= 	R.AccountNo AND
			   	I.AirlineID 	= 	%s			AND
			   	I.FlightNo 		= 	%s;
		'''

	'''
		Description: 	Create a customer
		Parameters: 	Id INTEGER, Email VARCHAR, Password VARCHAR, CreditCardNo INTEGER
		Tested:			Works
	'''
	CREATE = \
		'''
			INSERT INTO Customer(Id, Email, Password, CreditCardNo, CreationDate, RepresentativeId)
			VALUES(%s, %s, %s, %s, NOW(), %s);
		'''

	'''
		Description: 	Update a customer's credit card
		Parameters: 	CreditCardNo INTEGER, Id INTEGER
		Tested:			Works
	'''
	EDIT = \
		'''
			UPDATE 		Customer
				SET 	CreditCardNo = %s
				WHERE 	Id = %s;
		'''

	'''
		Description: 	Delete a customer
		Parameters: 	Id INTEGER
		Tested:			Works
	'''
	DELETE = \
		'''
			DELETE FROM Customer
			WHERE Id = %s
		'''


'''
OVERVIEW
========
All work!
'''
class Employee:
	'''
		Description:	Return all employees
		Parameters:		None
		Tested:			Works
	'''
	GET_ALL = \
		'''
			SELECT * FROM Employee;
		'''

	'''
		Description:	Return all employee ids
		Parameters:		None
		Tested:			Nope

	'''
	GET_ALL_IDS = \
		'''
			SELECT Id FROM Employee;
		'''

	'''
		Description:	Return all nonmanager employees
		Parameters:		None
		Tested:			Not yet
	'''
	GET_ALL_NONMANAGERS = \
		'''
			SELECT * FROM Employee
			WHERE IsManager = 0;
		'''

	'''
		Description:	Return an employee based on Id
		Parameters:		Id INTEGER
		Tested:			Works
	'''
	GET = \
		'''
			SELECT * FROM Employee
			WHERE Id = %s;
		'''

	'''
		Description: 	Get list of customers that employee represents
		Parameters:		RepresentativeId = %s
		Tested:			Not yet
	'''
	GET_CUSTOMERS = \
		'''
			SELECT 	Id FROM Customer
			WHERE 	RepresentativeId = %s
		'''

	'''
		Description:	Get a custom list of all customers that this employee represents
		Parameters:		Id
		Tested:			Nope
	'''
	GET_MAILING_LIST = \
		'''
			SELECT 	C.AccountNo, P.FirstName, P.LastName, C.Email, C.Rating
			FROM 	Customer C, Person P
			WHERE   C.RepresentativeId 	= %s AND
					P.Id = (SELECT 	Id
							FROM 	Customer
							WHERE 	RepresentativeId = %s);
		'''

	'''
		Description:	Add an employee
		Parameters:		Id INTEGER, SSN INTEGER, IsManager BOOLEAN, StartDate DATE, HourlyRate INTEGER
		Tested:			Works
	'''
	ADD = \
		'''
			INSERT INTO Employee (Id, SSN, IsManager, StartDate, HourlyRate)
			VALUES (%s, %s, %s, %s, %s);
		'''

	'''
		Description:	Edit an employee's information
		Parameter:		Id INTEGER, UserSSN INTEGER, IsManager BOOLEAN, StartDate DATE, HourlyRate INTEGER
		Tested:			Works
	'''
	EDIT = \
		'''
			UPDATE 	Employee
			SET 	Id 			= %s,
					IsManager 	= %s,
					StartDate 	= %s,
					HourlyRate 	= %s
 			WHERE 	SSN 		= %s;
		'''

	'''
		Description:	Delete an employee based on SSN
		Parameters:		Id INTEGER
		Tested:			Works
	'''
	DELETE = \
		'''
			DELETE FROM Employee
			WHERE Id = %s;
		'''

	'''
		Description:
		Parameters:
		Tested:
	'''
	GET_MAILING_LIST = \
		'''
			SELECT 	C.AccountNo, P.FirstName, P.LastName, C.Email, C.Rating
			FROM 	Customer C, Person P
			WHERE 	C.Id = P.Id AND C.RepresentativeId = %s
		'''


'''
OVERVIEW
========
GET_MULTICITY 			Not tested yet
GET_FLIGHTS_TIMERANGE 	Not tested yet
EDIT					Not tested yet
GET_MOST_ACTIVE 		Does not work... logically incorrect
'''
class Flight:

	'''
		Description: 	Get all flights
		Parameters: 	None
		Tested:			Works
	'''
	GET_ALL = \
		'''
			SELECT * FROM Flight
		'''

	'''
		Description: 	Get a single flight
		Parameters: 	FlightNo INTEGER
		Tested:			Works
	'''
	GET = \
		'''
			SELECT * FROM Flight
			WHERE FlightNo = %s;
		'''

	GET_ALL_AIRPORT = \
		'''
			SELECT * FROM Flight;
			WHERE F
		'''

	'''
		Desription:		Get all flight numbers
		Parameters:		None
		Tested:			Nope
	'''
	GET_ALL_FLIGHTNO = \
		'''
			SELECT FlightNo FROM Flight;
		'''

	'''
		Description:	Get all airlineIDs
		Parameters:		None
		Tested:			Nope
	'''
	GET_ALL_AIRLINEID = \
		'''
			SELECT Id FROM Airline;
		'''

	'''
		Description:	Make a suggested list of flights for a customer
		Parameters:		AccountNo INTEGER, AccountNo INTEGER
		Tested:			Works
	'''
	GET_SUGGESTED_LIST = \
		'''
			SELECT *
			FROM 	Flight F, Includes I
			WHERE 	F.FlightNo 	= I.FlightNo AND
					I.ResrNo 	= (
						SELECT 	ResrNo FROM Reservation R, Customer C
						WHERE 	C.AccountNo = %s AND
								R.AccountNo = %s
					);
		'''

	'''
		Description:	Used for one way flights... all outgoing
		Parameters:		DepAirportId CHAR, ArrAirportId CHAR, Departure DATE, NoOfSeats INTEGER
		Tested:
	'''
	GET_OUTGOING = \
		'''
			SELECT 	F.*, L.DepTime, L.ArrTime, L.LegNo
			FROM 	Flight F, Leg L
			WHERE 	L.DepAirportId 						 = %s AND
					L.ArrAirportId 						 = %s AND
					DATE(L.DepTime)	 					 = %s AND

					F.FlightNo 							 = L.FlightNo AND
					F.AirlineID 						 = L.AirlineID AND
					F.NoOfSeats 						>= %s;
		'''

	'''
		Description:	Used to get trips that leave and return to an airport
		Parameters:		DepAirportId CHAR, ArrAirportId CHAR, Departure DATE, ArrAirportId CHAR, DepAirportId CHAR, Return DATE, NoOfSeats INTEGER
		Tested:
	'''
	GET_ROUNDTRIP = \
		'''
			SELECT 	F.*, L1.DepTime, L1.ArrTime, L2.DepTime, L2.ArrTime, L1.LegNo, L2.LegNo
			FROM 	Leg L1, Leg L2, Flight F
			WHERE 	L1.DepAirportId 	= %s AND
					L1.ArrAirportId 	= %s AND
					DATE(L1.DepTime)	= %s AND

					L2.DepAirportId 	= %s AND
					L2.ArrAirportId 	= %s AND
					DATE(L2.DepTime)	= %s AND

					F.FlightNo 			= L1.FlightNo AND
					F.FlightNo 			= L2.FlightNo AND
					F.AirlineID 		= L1.AirlineID AND
					F.AirlineID 		= L2.AirlineID AND
					F.NoOfSeats	   		>= %s;
		'''

	'''
		Description:	Used to get all of the flights that have more than one leg
		Parameters:		AirportId INTEGER, NoOfSeats INTEGER, Departure DATE, NoOfSeats INTEGER
		Tested:			NOPE NOT YET
	'''
	GET_MULTICITY = \
		'''
			SELECT 	L1.*, L2.*, L3.*
			FROM 	Leg L1, Leg L2, Leg L3
			WHERE 	L1.FlightNo 	= L2.FlightNo 		AND
					L2.FlightNo 	= L3.FlightNo 		AND

					L1.DepAirportID = %s 		  		AND
					L1.DepAirportID = L2.ArrAirportID 	AND
					L2.DepAirportID = L3.ArrAirportId   AND

					DATE(L1.DepTime) 	= %s AND
					DATE(L2.ArrTime)	= %s;
		'''

	'''
		Description:	Get the flights from the current airportId going to an airport in the same city
		Parameters:		AirportId INTEGER
		Tested:			Works
	'''
	GET_DOMESTIC = \
		'''
			SELECT 	L.*
			FROM  	Airport A1, Airport A2, Leg L
			WHERE 	L.DepAirportID 	= %s AND
					A1.ID 			= L.DepAirportID AND
					A2.ID 			= L.ArrAirportID AND
					A1.Country 		= A2.Country;
		'''

	'''
		Description:	Get the international flights with a different country than the origin
		Parameters:		AirportId INTEGER
		Tested:			Works
	'''
	GET_INTERNATIONAL = \
		'''
			SELECT L.*
			FROM  Airport A1, Airport A2, Leg L
			WHERE L.DepAirportID = %s AND A1.ID = L.DepAirportID AND A2.ID = L.ArrAirportID AND A1.Country != A2.Country

		'''

	'''
		Description:	Produce a list of flights for an airport
		Parameters:		AirportId INTEGER, AirportId INTEGER
		Tested:			Works
	'''
	GET_BY_AIRPORT = \
		'''
			SELECT 	L.AirlineID, L.FlightNo, L.LegNo
			FROM 	Leg L
			WHERE 	L.DepAirportID 	=	%s 	OR
					L.ArrAirportID 	=	%s;
		'''

	'''
		Description:	Produce list of flights between a start and end time from an airport
		Parameters:		AirportId INTEGER, StartDatetime DATETIME, EndDatetime DATETIME
		Tested:			Nope, haven't tested
	'''
	GET_FLIGHTS_TIMERANGE = \
		'''
			SELECT 	*
			FROM 	Leg L
			WHERE 	L.DepAirportID 	= 	AirportID 		AND
					L.DepTime 		>= 	StartDatetime 	AND
					L.DepTime 		<= 	EndDatetime;
		'''

	'''
		Description: 	Flights that are on time
		Parameters:		None
		Tested:			Works
	'''
	GET_ON_TIME = \
		'''
			SELECT L.AirlineID,L.FlightNo,L.LegNo
			FROM Leg L
			WHERE L.ArrTime>=CURRENT_TIMESTAMP
		'''

	'''
		Description:	All of the flights that are delayed
		Parameters:		None
		Tested:			Works
	'''
	GET_DELAYED = \
		'''
			SELECT L.AirlineID,L.FlightNo,L.LegNo
			FROM Leg L
			WHERE L.ArrTime<CURRENT_TIMESTAMP
		'''

	'''
		Description:	Most active flights
		Parameters:		None
		Tested:			Does not work
	'''
	GET_MOST_ACTIVE = \
		'''
			SELECT AID,FN,MAX(y.num)
	    	FROM (SELECT AirlineID AS AID,FlightNo AS FN, COUNT(FlightNo) AS num
	          FROM Leg);
		'''

	'''
		Description:	Get legs for a flight
		Parameters:		FlightNo
		Tested:			Works
	'''
	GET_LEGS = \
		'''
			SELECT * FROM Leg
			WHERE FlightNo = %s;
		'''

	'''
		Description:	Get a list of flights by CustomerId
		Parameters:		AccountNo
		Tested:
	'''
	GET_BY_CUSTOMER = \
		'''
			SELECT * FROM Flight WHERE FlightNo = (
				SELECT FlightNo FROM Includes
				WHERE ResrNo = (SELECT ResrNo FROM Reservation WHERE AccountNo = %s)
			)
		'''

	'''
		Description: 	Get hidden fare for flight
		Parameters:		ResrNo INTEGER
	'''
	GET_HIDDEN_FARE = \
		'''
			SELECT HiddenFare FROM Flight
			WHERE FlightNo = (SELECT FlightNo FROM Includes WHERE ResrNo = %s AND LegNo = 1);

		'''

	'''
		Description: 	Create a new flight
		Parameters: 	AirlineId INTEGER, FlightNo INTEGER, NoOfSeats INTEGER, DaysOperating VARCHAR, MinLengthOfStay INTEGER, MaxLengthOfStay INTEGER
		Tested:			Works
	'''
	CREATE = \
		'''
			INSERT INTO Flight(AirlineId, FlightNo, NoOfSeats, DaysOperating, MinLengthOfStay, MaxLengthOfStay)
			VALUES (%s, %s, %s, %s, %s, %s);
		'''

	'''
		Description: 	Edit an existing flight
		Parameters: 	AirlineId INTEGER, NoOfSeats INTEGER, DaysOperating VARCHAR, MinLengthOfStay INTEGER, MaxLengthOfStay INTEGER, FlightNo INTEGER
		Tested:			Works
	'''
	EDIT = \
		'''
			UPDATE Flight
				SET AirlineId 		= %s,
				SET NoOfSeats 		= %s,
				SET DaysOperating 	= %s,
				SET MinLengthOfStay = %s,
				SET MaxLengthOfStay = %s
			WHERE 	FlightNo 		= %s;
		'''

	'''
		Description: 	Delete an existing flight
		Parameters: 	FlightNo INTEGER
		Tested:			Works
	'''
	DELETE = \
		'''
			DELETE FROM Flight
			WHERE FlightNo = %s;
		'''


'''
OVERVIEW
========
EDIT 					Not tested
DELETE 					Doesn't work - need to make sure we add cascade delete cause it has foreign keys
'''
class Reservation:
	'''
		Description:	Get every reservation
		Tested:			Works
	'''
	GET_ALL = \
		'''
			SELECT *
			FROM 	Reservation;
		'''

	'''
		Description:	Gets any reservations from a date in the past
		Parameters:		AccountNo INTEGER
		Tested:			Works
	'''
	GET_PAST = \
		'''
			SELECT 	*
			FROM 	Reservation R
			WHERE 	R.AccountNo = %s AND
					R.ResrNo IN (SELECT I.ResrNo FROM Includes I WHERE I.FlightNo IN (
									SELECT L.FlightNo FROM Leg L WHERE L.DepTime <= NOW() AND L.LegNo = 1));
		'''

	'''
		Description: 	Get a list of reservations based on customer number
		Parameters: 	AccountNo INTEGER
		Tested:			Works
	'''
	GET_BY_CUSTOMER = \
		'''
			SELECT  *
			FROM 	Reservation
			WHERE 	AccountNo = %s;
		'''

	'''
		Description:	Get a list of reservations that have been rejected and are current
		Parameters:		AccountNo INTEGER
	'''
	GET_BY_CUSTOMER_PENDING = \
		'''
			SELECT 	*
			FROM 	Reservation R
			WHERE 	R.AccountNo = %s 	AND
					(R.ResrNo IN (SELECT B.ResrNo FROM BidHistory B WHERE B.Accepted = 0) OR
					R.ResrNo NOT IN (SELECT B.ResrNo FROM BidHistory B WHERE B.Accepted = 1)) AND
					R.ResrNo IN (SELECT I.ResrNo FROM Includes I WHERE I.FlightNo IN (
									SELECT L.FlightNo FROM Leg L WHERE L.DepTime >= NOW() AND L.LegNo = 1));


		'''

	'''
		Description:	Get a list of reservations that have been bid on for a customer
		Parameters:		AccountNo INTEGER
	'''
	GET_BY_CUSTOMER_ACCEPTED = \
		'''
			SELECT 	*
			FROM 	Reservation R
			WHERE 	R.AccountNo = %s 	AND
					R.ResrNo IN (SELECT B.ResrNo FROM BidHistory B WHERE B.Accepted = 1) AND
					R.ResrNo IN (SELECT I.ResrNo FROM Includes I WHERE I.FlightNo IN (
									SELECT L.FlightNo FROM Leg L WHERE L.DepTime >= NOW() AND L.LegNo = 1));
		'''

	'''
		Description:	Get a list of reservation based on the flight number
		Parameters:		AirlineID INTEGER, FlightNo INTEGER
		Tested:			Works
	'''
	GET_BY_FLIGHT = \
		'''
			SELECT  *
			FROM 	Reservation R, Includes I
			WHERE  	R.ResrNo 		= I.ResrNo 		AND
					I.AirlineID   	= %s 			AND
					I.FlightNo 		= %s;
		'''

	'''
		Description:	Gets itinerary for a customer
		Parameters:		ResrNo INTEGER, ResrNo INTEGER, AccountNo INTEGER
		Tested:			Works
	'''
	GET_TRAVEL_ITINERARY = \
		'''
			SELECT 	L.*
			FROM 	Leg L, Includes I, Reservation R
			WHERE 	L.AirlineID = I.AirlineID AND
					L.FlightNo 	= I.FlightNo AND
					L.LegNo 	= I.LegNo AND
					I.ResrNo 	= %s AND
					R.ResrNo 	= %s AND
					R.AccountNo = %s;
		'''

	'''
		Description: 	Create a reservation for a flight
		Parameters:		ResrNo INTEGER, BookingFee FLOAT, RepId INTEGER, AccountNo INTEGER
		Tested:			Works
	'''
	CREATE = \
		'''
			INSERT INTO Reservation (ResrDate, BookingFee, RepId, AccountNo)
			VALUES (NOW(), %s, %s, %s);
		'''

	'''
		Description: 	Continuation of p1
		Parameters: 	ResrNo INTEGER, AirlineID CHAR, FlightNo INTEGER, LegNo INTEGER
		Tested:			Works
	'''
	CREATE_PART2 = \
		'''
			INSERT INTO Includes (ResrNo, AirlineID, FlightNo, LegNo, Date)
			VALUES (%s, %s, %s, %s, NOW());
		'''

	'''
		Description: 	Edit an existing reservation
		Parameters: 	ResrNo INTEGER, BookingFee FLOAT, RepId INTEGER, AccountNo INTEGER
		Tested:			Nah, didn't bother
	'''
	EDIT = \
		'''
			UPDATE Reservation
				SET BookingFee 	= %s,
				SET RepId 		= %s,
				SET AccountNo 	= %s
			WHERE 	ResrNo 		= %s;
		'''

	'''
		Desription:		Cancel a reservation
		Parameters:		ResrNo INTEGER, AccountNo INTEGER
		Tested:			Works, but need to add cascading delete
	'''
	DELETE = \
		'''
			DELETE  FROM Reservation
			WHERE 	ResrNo 		= %s
			AND 	AccountNo 	= %s;
		'''

	'''
		Desription:		Delete bid history for reservation
		Parameters:		ResrNo INTEGER
		Tested:			Works
	'''
	DELETE_BIDS = \
		'''
			DELETE FROM BidHistory
			WHERE ResrNo = %s;
		'''

	'''
		Desription:		Delete bid history for reservation
		Parameters:		ResrNo INTEGER
		Tested:			Works
	'''
	DELETE_INCLUDES = \
		'''
			DELETE FROM Includes
			WHERE ResrNo = %s;
		'''

'''
OVERVIEW
========
All work!
'''
class Airport:
	'''
		Description:	Return a list of all airports
		Parameters:		None
		Tested:			Works
	'''
	GET_ALL = \
		'''
			SELECT 	*
			FROM 	Airport;
		'''

	'''
		Description:	Return the airport ids
		Parameters:		None
		Tested:			NOPE
	'''
	GET_IDS =\
		'''
			SELECT 	Id
			FROM 	Airport;
		'''

	'''
		Description:	Return a list of all airports with this AirportId
		Parameters:		AirportId CHAR
		Tested:			Works
	'''
	GET = \
		'''
			SELECT * FROM Airport
			WHERE Id = %s;
		'''

	'''
		Description:	Return all cities
		Parameters:		None
		Tested:			Not yet
	'''
	GET_CITIES = \
		'''
			SELECT DISTINCT City FROM Airport;
		'''

	'''
		Description:	Add a new airport
		Parameters:		Id CHAR(5), Name CHAR(30), City CHAR(20), Country CHAR(30)
		Tested:			Works
	'''
	ADD = \
		'''
			INSERT INTO Airport (Id, Name, City, Country)
			VALUES (%s, %s, %s, %s);
		'''

	'''
		Description:	Edit an already added airport
		Parameters:		Name CHAR(30), City CHAR(20), Country CHAR(30), Id CHAR(5)
		Tested:			Works
	'''
	EDIT = \
		'''
		UPDATE 	Airport
			SET 	Name 	= %s,
					City 	= %s,
					Country = %s
 			WHERE 	Id 		= %s;
		'''

	'''
		Description:	Delete an airport
		Parameters:		Id CHAR(5)
		Tested:			Works
	'''
	DELETE = \
		'''
			DELETE FROM Airport
			WHERE Id = %s;
		'''

class Revenue:
	'''
		Description:	Return the revenue
		Parameters:		None
		Tested:			Nope
	'''
	GET_ALL = \
		'''
			SELECT 	SUM(B.Bid) + SUM(R.BookingFee) as TotalRev
			FROM 	BidHistory B, Reservation R
			WHERE 	B.Accepted = 1;

		'''

	'''
		Description:	Return all money made based on month and year
		Parameters:		DateBought DATETIME, DateReserved DATETIME
		Tested:			Nope
	'''
	GET_BY_MONTHINYEAR = \
		'''
			SELECT 	B.BidId, B.Bid, B.DateOfBid, R.BookingFee, R.RepId
			FROM 	BidHistory B, Reservation R, Includes I
			WHERE 	B.Accepted = 1 				AND
					I.ResrNo = R.ResrNo 		AND
					B.ResrNo = I.ResrNo 		AND
					MONTH(B.DateOfBid) 	= %s 	AND
					MONTH(R.ResrDate)  	= %s	AND
					YEAR(B.DateOfBid) 	= %s  	AND
					YEAR(R.ResrDate)	= %s;
		'''

	'''
		Description:	Return all money made on a bid
		Parameters:		BidID INTEGER
	'''
	GET_BY_BID= \
		'''
			SELECT 	Bid
			FROM 	BidHistory
			WHERE 	BidID = %s;
		'''

	'''
		Description:	Return all money made based on a flightno and airlineID
		Parameters:		FlightNo INTEGER, AirlineID CHAR(2)
		Tested:			Nope
	'''
	GET_BY_FLIGHT = \
		'''
			SELECT 	SUM(B.Bid) + SUM(R.BookingFee) as BidTotal
			FROM 	BidHistory B, Reservation R
			WHERE 	B.ResrNo IN (SELECT I.ResrNo FROM Includes I WHERE I.AirlineID = %s AND I.FlightNo = %s) AND
					B.Accepted = 1 AND
					B.ResrNo = R.ResrNo;
		'''

	'''
		Description:	Return all revenue based on an city
		Parameters:		City CHAR(16)
		Tested:			Nope
	'''
	GET_BY_DESTINATION = \
		'''
			SELECT 	SUM(B.Bid) + SUM(R.BookingFee) as BidTotal
			FROM 	BidHistory B, Reservation R
			WHERE 	B.ResrNo IN
					(SELECT I.ResrNo FROM Includes I WHERE I.FlightNo IN
						(SELECT L.FlightNo FROM Leg L WHERE DepAirportId IN
							(SELECT A.Id FROM Airport A WHERE City = %s)))
					AND
					R.ResrNo = B.ResrNo;
		'''

	'''
		Description:	Return all revenue for a customer
		Parameters:		AccountNo INTEGER
		Tested:			Nope
	'''
	GET_BY_CUSTOMER = \
		'''
			SELECT 	SUM(B.Bid) + SUM(R.BookingFee) as BidTotal
			FROM 	BidHistory B, Reservation R
			WHERE	B.Accepted 	= 1  AND
					R.AccountNo = %s AND
					R.ResrNo 	= B.ResrNo;
		'''

class Bid:
	'''
		Description:	Return the bid history table
		Parameters:		None
	'''
	GET_ALL = \
		'''
			SELECT * FROM BidHistory;
		'''

	'''
		Description:	Create a new bid
		Parameters:		Bid FLOAT, Accepted TINYINT (a 1 or 0), ResrNo INTEGER
	'''
	ADD = \
		'''
			INSERT INTO BidHistory(Bid, Accepted, DateOfBid, ResrNo)
			VALUES (%s, %s, NOW(), %s);
		'''


	'''
		Description:	Select a BidHistory element based on id
		Parameters:		BidID INTEGER
	'''
	GET = \
		'''
			SELECT * FROM BidHistory WHERE BidID = %s;
		'''

	'''
		Description:	Get all bids based on a customer
		Parameters:		AccountNo INTEGER
	'''
	GET_BY_ACCOUNTNO = \
		'''
			SELECT * FROM BidHistory B
			WHERE B.ResrNo IN (SELECT R.ResrNo FROM Reservation R WHERE AccountNo = %s);
		'''

