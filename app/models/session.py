from app import app
from flask import (
	redirect,
	session,
)
from functools import wraps

class Session:
	'''
		Session Keys
	'''
	K_USER 			= 'user'
	K_PERSON 		= 'person'
	K_EMPLOYEE		= 'employee'
	K_LOGGED_IN 	= 'logged_in'
	K_SOURCE_PAGE 	= 'source_page'
	K_TEMP			= 'temp_data'

	'''
		Accessors
	'''
	@classmethod
	def user(cls):
		return session.get(cls.K_USER, {})

	@classmethod
	def person(cls):
		return cls.user().get(cls.K_PERSON, {})

	@classmethod
	def employee(cls):
		return cls.user().get(cls.K_EMPLOYEE, {})

	@classmethod
	def logged_in(cls):
		return cls.user().get(cls.K_LOGGED_IN, False)

	@classmethod
	def is_manager(cls):
		return cls.employee().get('IsManager', False)

	@classmethod
	def is_employee(cls):
		return len(cls.employee())

	@classmethod
	def redirect_to_source(cls):
		source = session.get(cls.K_SOURCE_PAGE, '/')
		session[cls.K_SOURCE_PAGE] = '/'
		return redirect(source)

	'''
		Mutators
	'''
	@classmethod
	def temp(cls, data=None):
		if data:
			session[cls.K_TEMP] = data
		return session.get(cls.K_TEMP, {})

	@classmethod
	def login(cls, user, person, employee):
		user[cls.K_LOGGED_IN] 	= True
		session[cls.K_USER] 	= user
		session[cls.K_USER][cls.K_PERSON] = person
		session[cls.K_USER][cls.K_EMPLOYEE] = employee

	@classmethod
	def logout(cls):
		cls.user()[cls.K_LOGGED_IN] = False

	'''
		Decorators
	'''
	@classmethod
	def require_login(cls, source):
		def middle(func):
			@wraps(func)
			def inner(*args, **kwargs):
				if not cls.logged_in():
					session[cls.K_SOURCE_PAGE] = source
					return redirect('/login')
				return func(*args, **kwargs)
			return inner
		return middle

	@classmethod
	def require_logged_out(cls, dest):
		def middle(func):
			@wraps(func)
			def inner(*args, **kwargs):
				if cls.logged_in():
					return redirect(dest)
				return func(*args, **kwargs)
			return inner
		return middle

	@classmethod
	def require_employee(cls):
		def middle(func):
			@wraps(func)
			def inner(*args, **kwargs):
				if not cls.logged_in() or not cls.is_employee():
					return redirect('/')
				return func(*args, **kwargs)
			return inner
		return middle

	@classmethod
	def require_manager(cls):
		def middle(func):
			@wraps(func)
			def inner(*args, **kwargs):
				if not cls.logged_in() or not cls.is_manager():
					return redirect('/')
				return func(*args, **kwargs)
			return inner
		return middle