from app.models import data
import sys, inspect
from flask_wtf import FlaskForm
from wtforms import (
	BooleanField,
	DecimalField,
	FormField,
	FieldList,
	IntegerField,
	PasswordField,
	RadioField,
	SelectField,
	StringField,
	SubmitField,
)
from wtforms.fields.html5 import DateField
from wtforms.validators import (
	DataRequired,
	Email,
	EqualTo,
	Length,
	Required,
)

class LoginForm(FlaskForm):
	email = StringField('Email', validators=[DataRequired(), Email()])
	password = PasswordField('Password', validators=[DataRequired()])
	login = SubmitField('Login')
	register = SubmitField('Register')

class AccountDataForm(FlaskForm):
	firstName = StringField('First Name', validators=[DataRequired()])
	lastName = StringField('Last Name', validators=[DataRequired()])
	streetAddress = StringField('Street Address', validators=[DataRequired()])
	city = StringField('City', validators=[DataRequired()])
	state = StringField('State', validators=[DataRequired()])
	zipCode = StringField('Zip Code', validators=[DataRequired()])
	creditCard = StringField('Credit Card',validators=[DataRequired()])
	email = StringField('Email', validators=[DataRequired(), Email()])
	accountNo = IntegerField('Account Number')

class AccountForm(AccountDataForm):
	ssn = StringField('SSN')
	startDate = StringField('Start Date')
	hourlyRate = StringField('Hourly Rate')

	accounts = SelectField('Accounts')
	switchAccounts = SubmitField('Switch Accounts')

	save = SubmitField('Save Changes')
	delete = SubmitField('Delete Account')

class RegisterForm(AccountDataForm):
	password = PasswordField('Password', validators=
		[DataRequired(), EqualTo('confirm', message='Passwords must match'),
		Length(min=8, max=24)]
	)
	confirm = PasswordField('Confirm Password', validators=[DataRequired()])
	register = SubmitField('Register')

class FlightEntryForm(FlaskForm):
	destination = SelectField('Destination', choices=[('null', 'null')], validators=[DataRequired()])
	departing = DateField('Departing', validators=[DataRequired()])

class OneWaySearchForm(FlaskForm):
	origin = SelectField('Origin', choices=[('null', 'null')], validators=[DataRequired()])
	flights = FieldList(FormField(FlightEntryForm), min_entries=1)
	numPass = IntegerField('Number of Passengers', validators=[DataRequired()])
	search = SubmitField('Search')

class RoundTripSearchForm(FlaskForm):
	origin = SelectField('Origin', choices=[('null', 'null')], validators=[DataRequired()])
	flights = FieldList(FormField(FlightEntryForm), min_entries=1)
	numPass = IntegerField('Number of Passengers', validators=[DataRequired()])
	returning = DateField('Returning',validators=[DataRequired()])
	search = SubmitField('Search')

class MultiCitySearchForm(FlaskForm):
	origin = SelectField('Origin', choices=[('null', 'null')], validators=[DataRequired()])
	flights = FieldList(FormField(FlightEntryForm), min_entries=2)
	numPass = IntegerField('Number of Passengers', validators=[DataRequired()])
	add_flight = SubmitField('Add Flight')
	search = SubmitField('Search')

class TestDBForm(FlaskForm):
	_classes = [obj for _, obj in inspect.getmembers(data) if inspect.isclass(obj)]
	_statements = []

	for c in _classes:
		for v in vars(c):
			if v.startswith('__'):
				continue

			_name = str(c.__name__) + '.' + str(v)
			_statements.append((_name, _name))

	procs = SelectField('Procedure',
		choices=[('query', 'Query'), ('transact', 'Transaction')],
	)
	statements = SelectField('Statement', choices=_statements)
	args = StringField('Arguments')
	execute = SubmitField('Execute')


class SelectFlightForm(FlaskForm):
	flight = SelectField('Pick a Flight')

class ReserveForm(FlaskForm):
	flights = FieldList(FormField(SelectFlightForm), min_entries=1)
	accounts = SelectField('Account')
	reserve = SubmitField('Reserve')

class ByCustomerForm(FlaskForm):
	accountNo = IntegerField('Customer', validators=[DataRequired()])
	search = SubmitField('Search')

class ByAirportForm(FlaskForm):
	airport = SelectField('Airport', validators=[DataRequired()])
	#on_time = BooleanField('On-Time', validators=[])
	#delayed = BooleanField('Delayed', validators=[])
	search = SubmitField('Search')

class CustomerManager(FlaskForm):
	airlineID = SelectField('Airline ID', validators=[DataRequired()])
	flightNo = SelectField('Flight No',validators=[DataRequired()])
	search = SubmitField('Search')

class ReservationManager(FlaskForm):
	airlineID = SelectField('Airline ID', validators=[DataRequired()])
	flightNo = SelectField('Flight No',validators=[DataRequired()])
	search = SubmitField('Search')

class CityRevenue(FlaskForm):
	destCity = SelectField('Destination City', validators=[])
	search = SubmitField('Search')

class FlightRevenue(FlaskForm):
	airlineID = SelectField('Airline ID', validators=[DataRequired()])
	flightNo = SelectField('Flight No', validators=[DataRequired()])
	search = SubmitField('Search')

class CustomerRevenue(FlaskForm):
	accountNo = IntegerField('Customer', validators=[DataRequired()])
	search = SubmitField('Search')

class SalesReport(FlaskForm):
	months = [(1, "January"), (2, "February"), (3, "March"), (4, "April"), (5, "May"),
		(6, "June"), (7, "July"), (8, "August"), (9, "September"), (10, "October"),
		(11, "November"), (12, "December")]

	month = SelectField("Month", choices=months)
	year = SelectField("Year", choices=[(i, i) for i in range(2017, 1990, -1)])
	search = SubmitField('Search')


# Reservations forms
class PendingReservations(FlaskForm):
	reservation = SelectField('Reservation', coerce=int)
	bid = DecimalField('Bid', validators=[DataRequired()])
	make_bid = SubmitField('Bid on Reservation')

class CurrentReservations(FlaskForm):
	reservation = SelectField('Reservation')
	cancel = SubmitField('Cancel Reservation')